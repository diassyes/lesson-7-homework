package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"lesson-7-practice/src/domain"
	rpcHTTP "lesson-7-practice/src/rpc/http"
	"lesson-7-practice/src/services/user"
	"os"
	"time"
)

func main() {
	userId := 2
	TestServicesSequential(userId) // final result will be in userSequential.json
	TestServicesConcurrent(userId) // final result will be in userConcurrent.json
}

// Receives user data sequentially
func TestServicesSequential(userId int) {
	defer TimeTrack(time.Now(), "Sequential")
	userSource := rpcHTTP.NewUserSource(userId)
	userService := user.NewService(userSource)

	user, err := userService.GetUser()
	if err != nil {
		panic(nil)
	}

	writeFile(user, "userSequential.json")
}

// Receives user data concurrently
func TestServicesConcurrent(userId int) {
	defer TimeTrack(time.Now(), "Concurrent")
	userSource := rpcHTTP.NewUserSource(userId)
	userService := user.NewServiceConcurrent(userSource)

	user, err := userService.GetUser()
	if err != nil {
		panic(nil)
	}

	writeFile(user, "userConcurrent.json")
}

func TimeTrack(startTime time.Time, msg string) {
	fmt.Printf("%s elapsed time: %s\n", msg, time.Since(startTime))
}

func writeFile(user *domain.User, fileName string) {
	userJson, err := json.MarshalIndent(user, "", "    ")
	if err != nil {
		panic(err)
	}
	err = ioutil.WriteFile(fileName, userJson, os.ModePerm)
	if err != nil {
		panic(err)
	}
}