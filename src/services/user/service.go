package user

import (
	"lesson-7-practice/src/domain"
	rpcHTTP "lesson-7-practice/src/rpc/http"
	"lesson-7-practice/src/services/album"
	"lesson-7-practice/src/services/post"
	"lesson-7-practice/src/services/todo"
)

type Service struct {
	IUserSource domain.IUserSource
}

func NewService(userSource domain.IUserSource) domain.IUserService {
	return &Service{
		IUserSource: userSource,
	}
}

func (service *Service) GetUser() (*domain.User, error) {

	user, err := service.IUserSource.GetUser()
	if err != nil {
		return nil, err
	}

	postSource := rpcHTTP.NewPostSource(user.Id)
	postService := post.NewService(postSource)
	postsPtr, err := postService.GetAllPosts()
	if err != nil {
		return nil, err
	}

	user.Posts = make([]domain.Post, len(*postsPtr))
	copy(user.Posts, *postsPtr)

	albumSource := rpcHTTP.NewAlbumSource(user.Id)
	albumService := album.NewService(albumSource)
	albumsPtr, err := albumService.GetAllAlbums()
	if err != nil {
		return nil, err
	}

	user.Albums = make([]domain.Album, len(*albumsPtr))
	copy(user.Albums, *albumsPtr)

	todoSource := rpcHTTP.NewTodoSource(user.Id)
	todoService := todo.NewService(todoSource)
	todosPtr, err := todoService.GetAllTodos()
	if err != nil {
		return nil, err
	}

	user.Todos = make([]domain.Todo, len(*todosPtr))
	copy(user.Todos, *todosPtr)

	return user, nil
}