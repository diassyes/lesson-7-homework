package user

import (
	"lesson-7-practice/src/domain"
	rpcHTTP "lesson-7-practice/src/rpc/http"
	"lesson-7-practice/src/services/album"
	"lesson-7-practice/src/services/post"
	"lesson-7-practice/src/services/todo"
	"sync"
)

type ServiceConcurrent struct {
	IUserSource domain.IUserSource
}

func NewServiceConcurrent(userSource domain.IUserSource) domain.IUserService {
	return &ServiceConcurrent{
		IUserSource: userSource,
	}
}

func (service *ServiceConcurrent) GetUser() (*domain.User, error) {

	user, err := service.IUserSource.GetUser()
	if err != nil {
		return nil, err
	}

	wg := sync.WaitGroup{}
	wg.Add(1)
	go func(user *domain.User, wg *sync.WaitGroup) {
		defer wg.Done()
		postSource := rpcHTTP.NewPostSource(user.Id)
		postService := post.NewServiceConcurrent(postSource)
		postsPtr, err := postService.GetAllPosts()
		if err != nil {
			panic(err)
		}
		user.Posts = make([]domain.Post, len(*postsPtr))
		copy(user.Posts, *postsPtr)
	}(user, &wg)

	wg.Add(1)
	go func(user *domain.User, wg *sync.WaitGroup) {
		defer wg.Done()
		albumSource := rpcHTTP.NewAlbumSource(user.Id)
		albumService := album.NewServiceConcurrent(albumSource)
		albumsPtr, err := albumService.GetAllAlbums()
		if err != nil {
			panic(err)
		}
		user.Albums = make([]domain.Album, len(*albumsPtr))
		copy(user.Albums, *albumsPtr)
	}(user, &wg)

	todoSource := rpcHTTP.NewTodoSource(user.Id)
	todoService := todo.NewService(todoSource)
	todosPtr, err := todoService.GetAllTodos()
	if err != nil {
		panic(err)
	}
	user.Todos = make([]domain.Todo, len(*todosPtr))
	copy(user.Todos, *todosPtr)

	wg.Wait()

	return user, nil
}
