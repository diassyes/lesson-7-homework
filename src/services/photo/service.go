package photo

import "lesson-7-practice/src/domain"

type Service struct {
	IPhotoSource domain.IPhotoSource
}

func NewService(photoSource domain.IPhotoSource) domain.IPhotoService {
	return &Service{
		IPhotoSource: photoSource,
	}
}

func (service *Service) GetAllPhotos() (*[]domain.Photo, error) {
	photos, err := service.IPhotoSource.GetAllPhotos()
	if err != nil {
		return nil, err
	}
	return photos, nil
}