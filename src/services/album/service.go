package album

import (
	"lesson-7-practice/src/domain"
	rpcHTTP "lesson-7-practice/src/rpc/http"
	"lesson-7-practice/src/services/photo"
)

type Service struct {
	IAlbumSource domain.IAlbumSource
}

func NewService(albumSource domain.IAlbumSource) domain.IAlbumService {
	return &Service{
		IAlbumSource: albumSource,
	}
}

func (service *Service) GetAllAlbums() (*[]domain.Album, error) {

	albums, err := service.IAlbumSource.GetAllAlbums()
	if err != nil {
		return nil, err
	}

	for i := 0; i < len(*albums); i++ {
		photoSource := rpcHTTP.NewPhotoSource((*albums)[i].Id)
		photoService := photo.NewService(photoSource)
		photoPtr, err := photoService.GetAllPhotos()
		if err != nil {
			return nil, err
		}
		(*albums)[i].Photos = make([]domain.Photo, len(*photoPtr))
		copy((*albums)[i].Photos, *photoPtr)
	}

	return albums, nil
}