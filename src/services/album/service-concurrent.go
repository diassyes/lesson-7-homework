package album

import (
	"lesson-7-practice/src/domain"
	rpcHTTP "lesson-7-practice/src/rpc/http"
	"lesson-7-practice/src/services/photo"
	"sync"
)

type ServiceConcurrent struct {
	IAlbumSource domain.IAlbumSource
}

func NewServiceConcurrent(albumSource domain.IAlbumSource) domain.IAlbumService {
	return &ServiceConcurrent{
		IAlbumSource: albumSource,
	}
}

func (service *ServiceConcurrent) GetAllAlbums() (*[]domain.Album, error) {

	albums, err := service.IAlbumSource.GetAllAlbums()
	if err != nil {
		return nil, err
	}

	wg := sync.WaitGroup{}
	for i := 0; i < len(*albums); i++ {
		wg.Add(1)
		go func(wg *sync.WaitGroup, album *domain.Album) {
			defer wg.Done()
			photoSource := rpcHTTP.NewPhotoSource(album.Id)
			photoService := photo.NewService(photoSource)
			photosPtr, err := photoService.GetAllPhotos()
			if err != nil {
				panic(err)
			}
			album.Photos = make([]domain.Photo, len(*photosPtr))
			copy(album.Photos, *photosPtr)
		}(&wg, &(*albums)[i])
	}
	wg.Wait()

	return albums, nil
}