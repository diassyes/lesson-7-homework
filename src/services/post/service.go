package post

import (
	"lesson-7-practice/src/domain"
	rpcHTTP "lesson-7-practice/src/rpc/http"
	"lesson-7-practice/src/services/comment"
)

type Service struct {
	IPostSource domain.IPostSource
}

func NewService(postSource domain.IPostSource) domain.IPostService {
	return &Service{
		IPostSource: postSource,
	}
}

func (service *Service) GetAllPosts() (*[]domain.Post, error) {

	posts, err := service.IPostSource.GetAllPosts()
	if err != nil {
		return nil, err
	}

	for i := 0; i < len(*posts); i++ {
		commentSource := rpcHTTP.NewCommentSource((*posts)[i].Id)
		commentService := comment.NewService(commentSource)
		commentsPtr, err := commentService.GetAllComments()
		if err != nil {
			return nil, err
		}
		(*posts)[i].Comments = make([]domain.Comment, len(*commentsPtr))
		copy((*posts)[i].Comments, *commentsPtr)
	}

	return posts, nil
}
