package post

import (
	"lesson-7-practice/src/domain"
	rpcHTTP "lesson-7-practice/src/rpc/http"
	"lesson-7-practice/src/services/comment"
	"sync"
)

type ServiceConcurrent struct {
	IPostSource domain.IPostSource
}

func NewServiceConcurrent(postSource domain.IPostSource) domain.IPostService {
	return &ServiceConcurrent{
		IPostSource: postSource,
	}
}

func (service *ServiceConcurrent) GetAllPosts() (*[]domain.Post, error) {

	posts, err := service.IPostSource.GetAllPosts()
	if err != nil {
		return nil, err
	}

	wg := sync.WaitGroup{}
	for i := 0; i < len(*posts); i++ {
		wg.Add(1)
		go func(wg *sync.WaitGroup, post *domain.Post) {
			defer wg.Done()
			commentSource := rpcHTTP.NewCommentSource(post.Id)
			commentService := comment.NewService(commentSource)
			commentsPtr, err := commentService.GetAllComments()
			if err != nil {
				panic(err)
			}
			post.Comments = make([]domain.Comment, len(*commentsPtr))
			copy(post.Comments, *commentsPtr)
		}(&wg, &(*posts)[i])
	}
	wg.Wait()

	return posts, nil
}