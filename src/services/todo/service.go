package todo

import (
	"lesson-7-practice/src/domain"
)

type Service struct {
	ITodoSource domain.ITodoSource
}

func NewService(todoSource domain.ITodoSource) domain.ITodoService {
	return &Service{
		ITodoSource: todoSource,
	}
}

func (service *Service) GetAllTodos() (*[]domain.Todo, error) {
	todos, err := service.ITodoSource.GetAllTodos()
	if err != nil {
		return nil, err
	}
	return todos, nil
}