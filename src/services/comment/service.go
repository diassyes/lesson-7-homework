package comment

import "lesson-7-practice/src/domain"

type Service struct {
	ICommentSource domain.ICommentSource
}

func NewService(commentSource domain.ICommentSource) domain.ICommentService {
	return &Service{
		ICommentSource: commentSource,
	}
}

func (service *Service) GetAllComments() (*[]domain.Comment, error) {

	comments, err := service.ICommentSource.GetAllComments()
	if err != nil {
		return nil, err
	}
	return comments, nil
}