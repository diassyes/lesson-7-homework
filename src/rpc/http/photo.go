package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"lesson-7-practice/src/domain"
	"net/http"
)

type PhotoSource struct {
	photoUrl string
}

func NewPhotoSource(postId int) domain.IPhotoSource {
	photoBaseUrl := "https://jsonplaceholder.typicode.com/albums/%d/photos"
	photoSource := PhotoSource{photoUrl: fmt.Sprintf(photoBaseUrl, postId)}
	return &photoSource
}

func(photoSource *PhotoSource) GetAllPhotos() (*[]domain.Photo, error) {
	var photos []domain.Photo
	response, err := http.Get(photoSource.photoUrl)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal([]byte(body), &photos)
	if err != nil {
		return nil, err
	}
	return &photos, err
}
