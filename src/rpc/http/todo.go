package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"lesson-7-practice/src/domain"
	"net/http"
)

type TodoSource struct {
	todoUrl string
}

func NewTodoSource(userId int) domain.ITodoSource {
	todoBaseUrl := "https://jsonplaceholder.typicode.com/user/%d/todos"
	todoSource := TodoSource{todoUrl: fmt.Sprintf(todoBaseUrl, userId)}
	return &todoSource
}

func(todoSource *TodoSource) GetAllTodos() (*[]domain.Todo, error) {
	var todos []domain.Todo
	response, err := http.Get(todoSource.todoUrl)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal([]byte(body), &todos)
	if err != nil {
		return nil, err
	}
	return &todos, err
}