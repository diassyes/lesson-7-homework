package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"lesson-7-practice/src/domain"
	"net/http"
)

type AlbumSource struct {
	albumUrl string
}

func NewAlbumSource(userId int) domain.IAlbumSource {
	albumBaseUrl := "https://jsonplaceholder.typicode.com/user/%d/albums"
	albumSource := AlbumSource{albumUrl: fmt.Sprintf(albumBaseUrl, userId)}
	return &albumSource
}

func(albumSource *AlbumSource) GetAllAlbums() (*[]domain.Album, error) {
	var albums []domain.Album
	response, err := http.Get(albumSource.albumUrl)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal([]byte(body), &albums)
	if err != nil {
		return nil, err
	}
	return &albums, err
}
