package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"lesson-7-practice/src/domain"
	"net/http"
)

type UserSource struct {
	userUrl string
}

func NewUserSource(userId int) domain.IUserSource {
	userUrl := "https://jsonplaceholder.typicode.com/users/%d"
	return &UserSource{userUrl: fmt.Sprintf(userUrl, userId)}
}

func(userSource *UserSource) GetUser() (*domain.User, error) {
	var user domain.User
	response, err := http.Get(userSource.userUrl)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal([]byte(body), &user)
	if err != nil {
		return nil, err
	}
	return &user, err
}
