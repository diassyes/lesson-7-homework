package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"lesson-7-practice/src/domain"
	"net/http"
)

type PostSource struct {
	postUrl string
}

func NewPostSource(userId int) domain.IPostSource {
	postBaseUrl := "https://jsonplaceholder.typicode.com/user/%d/posts"
	postSource := PostSource{postUrl: fmt.Sprintf(postBaseUrl, userId)}
	return &postSource
}

func(postSource *PostSource) GetAllPosts() (*[]domain.Post, error) {
	var posts []domain.Post
	response, err := http.Get(postSource.postUrl)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal([]byte(body), &posts)
	if err != nil {
		return nil, err
	}
	return &posts, err
}
