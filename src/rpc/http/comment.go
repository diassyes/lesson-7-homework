package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"lesson-7-practice/src/domain"
	"net/http"
)

type CommentSource struct {
	commentUrl string
}

func NewCommentSource(postId int) domain.ICommentSource {
	commentBaseUrl := "https://jsonplaceholder.typicode.com/posts/%d/comments"
	commentSource := CommentSource{commentUrl: fmt.Sprintf(commentBaseUrl, postId)}
	return &commentSource
}

func(commentSource *CommentSource) GetAllComments() (*[]domain.Comment, error) {
	var comments []domain.Comment
	response, err := http.Get(commentSource.commentUrl)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal([]byte(body), &comments)
	if err != nil {
		return nil, err
	}
	return &comments, err
}
