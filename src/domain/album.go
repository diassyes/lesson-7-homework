package domain

type Album struct {
	UserId int `json:"userId,omitempty"`
	Id     int `json:"id,omitempty"`
	Title  string `json:"title,omitempty"`

	Photos []Photo `json:"photos,omitempty"`
}

type IAlbumSource interface {
	GetAllAlbums() (*[]Album, error)
}

type IAlbumService interface {
	GetAllAlbums() (*[]Album, error)
}
