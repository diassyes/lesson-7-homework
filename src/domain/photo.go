package domain

type Photo struct {
	AlbumId      int    `json:"albumId,omitempty"`
	Id           int    `json:"id,omitempty"`
	Title        string `json:"title,omitempty"`
	Url          string `json:"url,omitempty"`
	ThumbnailUrl string `json:"thumbnailUrl,omitempty"`
}

type IPhotoSource interface {
	GetAllPhotos() (*[]Photo, error)
}

type IPhotoService interface {
	GetAllPhotos() (*[]Photo, error)
}