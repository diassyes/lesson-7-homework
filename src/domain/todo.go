package domain

type Todo struct {
	UserId    int    `json:"userId,omitempty"`
	Id        int    `json:"id,omitempty"`
	Title     string `json:"title,omitempty"`
	Completed bool   `json:"completed,omitempty"`
}

type ITodoSource interface {
	GetAllTodos() (*[]Todo, error)
}

type ITodoService interface {
	GetAllTodos() (*[]Todo, error)
}