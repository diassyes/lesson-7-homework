package domain

type Post struct {
	UserId int    `json:"userId,omitempty"`
	Id     int    `json:"id,omitempty"`
	Title  string `json:"title,omitempty"`
	Body   string `json:"body,omitempty"`

	Comments []Comment
}

type IPostSource interface {
	GetAllPosts() (*[]Post, error)
}

type IPostService interface {
	GetAllPosts() (*[]Post, error)
}