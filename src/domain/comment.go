package domain

type Comment struct {
	PostId int    `json:"postId,omitempty"`
	Id     int    `json:"id,omitempty"`
	Name   string `json:"name,omitempty"`
	Email  string `json:"email,omitempty"`
	Body   string `json:"body,omitempty"`
}

type ICommentSource interface {
	GetAllComments() (*[]Comment, error)
}

type ICommentService interface {
	GetAllComments() (*[]Comment, error)
}
